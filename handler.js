'use strict';

module.exports.hello = async (event, context) => {
  return {
    statusCode: 200,
    body: JSON.stringify({
      text: "let's hope it stays here..."
    }),
  };
};
